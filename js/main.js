import Vue from "vue/dist/vue.js";
import BootstrapVue from "bootstrap-vue";
import Vuelidate from 'vuelidate';
import Slick from "vue-slick";
import VueSlider from 'vue-slider-component';
import VueTheMask from 'vue-the-mask';
import VueTelInput from 'vue-tel-input';
import 'vue-slider-component/theme/antd.css'
import "slick-carousel/slick/slick.css";
const settings = {
  apiKey: '',
  lang: 'ru_RU',
  coordorder: 'latlong', 
  version: '2.1'
}
import YmapPlugin from 'vue-yandex-maps'
import { loadYmap } from 'vue-yandex-maps'
import { email, required, sameAs } from 'vuelidate/lib/validators'
Vue.use(Vuelidate);
Vue.use(VueTheMask);
Vue.use(VueTelInput);
Vue.use(YmapPlugin, settings);
Vue.use(BootstrapVue);


const SEARCH_BY_TAG = 1;
const SEARCH_BY_FILTER = 2;
const SEARCH_BY_MAP = 3;

Vue.directive("click-outside", {
  bind(el, binding, vnode) {
    var vm = vnode.context;
    var callback = binding.value;
    el.clickOutsideEvent = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        return callback.call(vm, event);
      }
    };
    document.body.addEventListener("click", el.clickOutsideEvent);
  },
  unbind(el) {
    document.body.removeEventListener("click", el.clickOutsideEvent);
  }
});

new Vue({
  el: "#app",
  watch: {
    searchText: function (val) {
      if (val) {
        if (this.searchType === this.searchTypes.SEARCH_BY_TAG) {
          this.searchResultsByTag = [];
          for (var i = 0; i < val.length + 1; i++) {
            this.searchResultsByTag.push(val + "-fake-data-" + i);
          }
        }
      } else {
        this.searchResultsByTag = [];
      }
    },
    searchTextAddress: function (val) {
      if (val) {
        if (this.searchType === this.searchTypes.SEARCH_BY_FILTER) {
          this.searchResultsByAddress = [];
          for (var i = 0; i < val.length + 1; i++) {
            this.searchResultsByAddress.push(val + "-fake-data-" + i);
          }
        }
      } else {
        this.searchResultsByAddress = [];
      }
    }
  },
  components: {
    Slick,
    VueSlider
  },
  validations: {
    email: { email, required },
    password: { required },
    repeatPassword: { required, sameAsPassword: sameAs('password')},
    phone: { required },
    code: { required },
    firstName: { required },
    lastName: { required },
  },
  data: {
    disabled: false,
    valueProgress: 60,
    minProgress: 0,
    maxProgress: 60,
    resend: false,
    phone: '',
    email: '',
    password:'',
    repeatPassword:'',
    code: '',
    firstName: '',
    lastName: '',
    selected: null,
    stepOne: true,
    stepSecond: false,
    loading: true,
    isHovering1: false,
    isHovering2: false,
    isHovering3: false,
    isHovering4: false,
    isHovering5: false,
    isHovering6: false,
    isHovering7: false,
    isHovering8: false,
    isHovering9: false,
    isHovering10: false,
    isHovering11: false,
    isHovering12: false,
    isHovering13: false,
    isHovering14: false,
    isHovering15: false,
    isHovering16: false,
    detailsShowing: true,
    controls: ['zoomControl'],
    hintLayout: '<div class="tooltipe-map skew"><span>128</span></div>',
    hintLayout2: '<div class="tooltipe-map skew"><span>120</span></div>',
    hintLayout3: '<div class="tooltipe-map skew"><span>132</span></div>',
    markerIcon: {
      layout: 'default#imageWithContent',
      imageHref: './img/icons/map-marker.svg',
      imageSize: [60, 60],
      imageOffset: [-30, -30]
    },
    placemarks: [
      {
        coords: [54.8, 39.8],
        properties: {}, // define properties here
        options: {}, // define options here
        clusterName: "1",
        callbacks: { click: function() {} }
      }
    ],
    mortgage1: false,
    mortgage2: false,
    mortgage3: false,
    mortgage4: false,
    showNav: false,
    slickOptions: {
      infinite: true,
      dots: false,
      speed: 300,
      swipeToSlide: true,
      prevArrow:
        '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><img src="img/icons/arr-slider-left.svg" alt=""></button>',
      nextArrow:
        '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><img src="img/icons/arr-slider-right.svg" alt=""></button>',
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
          }
        }
      ]
    },
    window: {
      width: 0,
      height: 0
    },
    searchText: "",
    searchTextAddress: "",
    searchType: SEARCH_BY_TAG,
    searchTypes: {
      SEARCH_BY_TAG,
      SEARCH_BY_FILTER,
      SEARCH_BY_MAP
    },
    searchTags: [],
    upToPrice: "",
    fromPrice: "",
    detailFilter: false,
    views: [      
      {
        isActive: true,
        name: "Общий вид",
        code: "common"
      },
      {
        isActive: false,
        name: "Сводный вид",
        code: "general"
      },      
      {
        isActive: false,
        name: "Табличный вид",
        code: "table"
      },
      {
        isActive: false,
        name: "Каталог планировок",
        code: "catalog"
      }
    ],
    max: 2,
    location: {
      code: "msk",
      name: "Москва"
    },
    locations: [
      {
        code: "msk",
        name: "Москва"
      },
      {
        code: "yr",
        name: "Урюпинск"
      }
    ],
    items: [
      {
        discount: "Информация о скидке! 20%",
        name: "ЖК Дон строй",
        company: "СМУ 134",
        title: "1-rомнатная квартира 32 м 19 этаж",
        address: "Каширское шоссе, д. 23, корп. 4 ",
        stations: [
          {
            name: "Юго-заподная",
            color: 'red'
          },
          {
            name: "Филевский парк",
            color: 'blue'
          },
          {
            name: "Алексеевская",
            color: 'red'
          },
        ],
        text:
          "Ипотека на выгодных условиях, поможем получить одобрение бесплатно. Продается квартира в г.Москва, ЮВАО, район Некрасовка, Люберецкие поля, кв. 13, к. 3, на 10 этаже, площадь 32, 4 кв.м... ",
        price: "273 000 млн ₽",
        affiliateValue: "21%",
        iconsInf: true,
        selectedPicture: {
          href: "",
          src: "./img/items/house1.jpg",
          alt: "image 1"
        },
        pictures: [
          {
            href: "",
            src: "./img/items/house6.jpg",
            alt: "image 1"
          },
          {
            href: "",
            src: "./img/items/house2.jpg",
            alt: "image 2"
          },
          {
            href: "",
            src: "./img/items/house3.jpg",
            alt: "image 3"
          },
          {
            href: "",
            src: "./img/items/house6.jpg",
            alt: "image 4"
          },
          {
            href: "",
            src: "./img/items/house5.jpg",
            alt: "image 5"
          }
        ]
      },
      {
        discount: "Информация о скидке! 20%",
        name: "ЖК Дон строй",
        company: "СМУ 134",
        title: "1-rомнатная квартира 32 м 19 этаж",
        address: "Каширское шоссе, д. 23, корп. 4 ",
        stations: [
          {
            name: "Юго-заподная",
            color: 'red'
          },
          {
            name: "Филевский парк",
            color: 'blue'
          },
          {
            name: "Алексеевская",
            color: 'red'
          },
        ],
        text:
          "Ипотека на выгодных условиях, поможем получить одобрение бесплатно. Продается квартира в г.Москва, ЮВАО, район Некрасовка, Люберецкие поля, кв. 13, к. 3, на 10 этаже, площадь 32, 4 кв.м... ",
        price: "273 000 млн ₽",
        affiliateValue: "21%",
        iconsInf: true,
        selectedPicture: {
          href: "",
          src: "./img/items/house1.jpg",
          alt: "image 1"
        },
        pictures: [
          {
            href: "",
            src: "./img/items/house1.jpg",
            alt: "image 1"
          },
          {
            href: "",
            src: "./img/items/house2.jpg",
            alt: "image 2"
          },
          {
            href: "",
            src: "./img/items/house3.jpg",
            alt: "image 3"
          },
          {
            href: "",
            src: "./img/items/house4.jpg",
            alt: "image 4"
          },
          {
            href: "",
            src: "./img/items/house5.jpg",
            alt: "image 5"
          }
        ]
      }
    ],
    offers: [
      {
        img: "./img/offers/1x/1.png",
        title: "Жилой комплекс «Столичные поляны»",
        price: "от 3, 34 млн ₽",
        address: "Улица Скобелевская 10 мин. пешком"
      },
      {
        img: "./img/offers/1x/1.png",
        title: "Жилой комплекс «Столичные поляны»",
        price: "от 3, 34 млн ₽",
        address: "Улица Скобелевская 10 мин. пешком"
      },
      {
        img: "./img/offers/1x/1.png",
        title: "Жилой комплекс «Столичные поляны»",
        price: "от 3, 34 млн ₽",
        address: "Улица Скобелевская 10 мин. пешком"
      },
      {
        img: "./img/offers/1x/1.png",
        title: "Жилой комплекс «Столичные поляны»",
        price: "от 3, 34 млн ₽",
        address: "Улица Скобелевская 10 мин. пешком"
      },
      {
        img: "./img/offers/1x/1.png",
        title: "Жилой комплекс «Столичные поляны»",
        price: "от 3, 34 млн ₽",
        address: "Улица Скобелевская 10 мин. пешком"
      }
    ],
    addressList: [
      { name: "Авиамоторная" },
      { name: "Волжская" },
      { name: "Крестьянская застава" },
      { name: "Новопеределкино" },
      { name: "Ржевская" },
      { name: "Тушинская" },
      { name: "Автозаводская" },
      { name: "Волоколамская" },
      { name: "Кропоткинская" },
      { name: "Новослободская" },
      { name: "Рижская" },
      { name: "Угрешская" },
      { name: "Академическая" },
      { name: "Воробьевы горы" },
      { name: "Крылатское" },
      { name: "Новохохловская" },
      { name: "Римская" },
      { name: "Улица 1905 года" },
      { name: "Александровский сад" },
      { name: "Крымская" },
      { name: "Новоясеневская" },
      { name: "Ростокино" },
      { name: "Улица 800-летия Москвы" },
      { name: "Алексеевская" },
      { name: "Выставочная" },
      { name: "Кузнецкий мост" },
      { name: "Новые Черемушки" },
      { name: "Рубцовская" },
      { name: "Улица Академика Королева" }
    ],
    searchActiveResultsByTag: [],
    searchActiveResultsByAddress: [],
    searchResultsByTag: [],
    searchResultsByAddress: [],
    rows: 234,
    currentPage: 1,
    currentFilterParam: "area",
    areas: {
      live: [
        {
          name: "Квартира",
          active: true
        },
        {
          name: "Дом",
          active: false
        },
        {
          name: "Комната",
          active: false
        },
        {
          name: "Дом",
          active: false
        },
        {
          name: "Часть дома",
          active: false
        },
        {
          name: "Дом",
          active: false
        },
        {
          name: "Часть дома",
          active: false
        },
        {
          name: "Комната",
          active: false
        },
        {
          name: "Часть дома",
          active: false
        }
      ],
      commercial: [
        {
          name: "Офис",
          active: false
        },
        {
          name: "Часть офиса",
          active: false
        },
        {
          name: "Подвал",
          active: false
        },
        {
          name: "Коридок",
          active: false
        },
        {
          name: "Часть дома",
          active: false
        },
        {
          name: "Дом",
          active: false
        }
      ]
    },
    developers: [
      {
        img: "./img/big-house.jpg",
        title: "Лидеры рынка",
        name: "Эталон Инвест",
        description: "Архитектура от ведущих бюро"
      }
    ],
    rangePrice: "0",
    startPrice: "0",
    age: "5",
    rangeOptions: {
      dotSize: 14,
      width: 'auto',
      height: 7
    },
    slickObjectBig: {
      infinite: true,
      dots: false,
      speed: 300,
      prevArrow:
        '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><img src="img/icons/arr-slider-left_white.svg" alt=""></button>',
      nextArrow:
        '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><img src="img/icons/arr-slider-right_white.svg" alt=""></button>',
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      asNavFor: ".slider-nav"
    },
    slickObjectSm: {
      infinite: true,
      dots: false,
      speed: 300,
      arrows: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      fade: false,
      asNavFor: ".slider-for",
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    },
    slickObject: {
      infinite: false,
      dots: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      swipe: false,
      drag: false,
      prevArrow:
        '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><img src="img/icons/arr-slider-left.svg" alt=""></button>',
      nextArrow:
        '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><img src="img/icons/arr-slider-right.svg" alt=""></button>',
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        }
      ]
    },
    slickObjectBigModal: {
      infinite: true,
      dots: false,
      speed: 300,
      prevArrow:
        '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><img src="img/icons/arr-slider-left_white.svg" alt=""></button>',
      nextArrow:
        '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><img src="img/icons/arr-slider-right_white.svg" alt=""></button>',
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      asNavFor: ".slider-nav-2"
    },
    slickObjectSmModal: {
      infinite: true,
      dots: false,
      speed: 300,
      arrows: false,
      slidesToShow: 10,
      slidesToScroll: 1,
      fade: false,
      asNavFor: ".slider-for-2",
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 1600,
          settings: {
            slidesToShow: 8,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 1400,
          settings: {
            slidesToShow: 7,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 1100,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    },
    timer: null,
    validStepTwo: false,
    user: {
      name: '',
      phone: ''
    },
    users: [
      {
        name: '',
        phone: ''
      }
    ],
    mainTimer: null,
    resetTimer: null,
    fav1: false,
    fav2: false,
    fav3: false,
    fav4: false,
    fav5: false,
    fav6: false,
    idModal: '',
  },
  computed: {
    addressOverflow() {
      return this.searchActiveResultsByAddress.join("").length > 10;
    },
    areasSelected() {
      return this.areas.live.concat(this.areas.commercial).filter(a => a.active);
    },
    areasSelectedFirst() {
      return this.areasSelected.length ? this.areasSelected[0].name : this.areas.live[0].name;
    },
    isMobile: function () {
      var check = false;
      (function (a) {
        if (
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
            a
          ) ||
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
            a.substr(0, 4)
          )
        )
          check = true;
      })(navigator.userAgent || navigator.vendor || window.opera);
      return check;
    },
    isMobileWidth: function () {
      return this.window.width <= 480;
    },
    useMobileFilter: function () {
      return this.window.width <= 767;
    },
    isMac: function () {
      var check = false;
      if (navigator.userAgent.indexOf('Mac OS X') != -1) {
        check = true;
      } else {
        check = false;
      }
      return check;
    },
    
    searchButtonTitle: function () {
      switch (this.searchType) {
        case this.searchTypes.SEARCH_BY_TAG:
          return "Поиск";
        case this.searchTypes.SEARCH_BY_FILTER:
          return "Поиск";
        default:
          return "Поиск";
      }
    }
  },
  methods: {
    del (index) {
      this.$delete(this.users, index)
    },
    addNewUser: function () {
      this.users.push(Vue.util.extend({}, this.users))
    },
    closeDetailFilter: function () {
      this.$nextTick(() => {
        if (this.detailFilter) {
          if (!this.useMobileFilter) {
            this.detailFilter = false;
          }
        }
      });
    },
    closeNav: function () {
      this.showNav = false;
    },
    setFilterParam: function (param) {
      this.currentFilterParam = param;
    },
    mobileCloseFromFilter: function () {
      this.showMainFilter(this.searchTypes.SEARCH_BY_TAG);
      this.detailFilter = false;
    },
    focusInput: function (type) {
      this.detailFilter = true;
      this.$nextTick(() => {
        this.$refs.search_input.focus();
      });
    },
    focusAddressInput: function (type) {
      this.detailFilter = true;
      this.$nextTick(() => {
        this.$refs.search_address_input.focus();
      });
    },
    showMainFilter: function (type) {
      this.searchType = type;
      this.detailFilter = true;
      this.searchText = "";
      this.searchTextAddress = "";

      // clear search form
      switch (this.searchType) {
        case this.searchTypes.SEARCH_BY_TAG:
          this.searchActiveResultsByTag = [];
          this.searchResultsByTag = [];
          break;
        case this.searchTypes.SEARCH_BY_FILTER:
          break;
      }
    },
    handleResize() {
      this.window.width = window.innerWidth;
      this.window.height = window.innerHeight;
    },
    deleteFilterResults: function (val) {
      if (!this.searchText) {
        this.searchActiveResultsByTag.splice(-1, 1);
      }
    },
    deleteFilterResultsAddress: function (val) {
      if (!this.searchTextAddress) {
        this.searchActiveResultsByAddress.splice(-1, 1);
      }
    },
    addToSearch: function (val) {
      if (val) {
        this.searchText = "";
        this.searchResultsByTag = this.searchResultsByTag.filter(t => t !== val);
        this.searchActiveResultsByTag.push(val);
      }
    },
    addToAddressFilter: function (val) {
      if (val && this.searchActiveResultsByAddress.length < 4) {
        this.searchTextAddress = "";
        this.searchResultsByAddress = this.searchResultsByAddress.filter(t => t !== val);
        this.searchActiveResultsByAddress.push(val);
      }
    },
    removeVariantFromTagSearch: function (index) {
      this.searchActiveResultsByTag.splice(index, 1);
    },
    removeVariantFromAddressSearch: function (index) {
      this.searchActiveResultsByAddress.splice(index, 1);
    },
    setView: function (view) {
      this.views.map(function (v) {
        v.isActive = view.code === v.code;
        return v;
      });
    },
    //slick
    next() {
      this.$refs.slick.next();
    },

    prev() {
      this.$refs.slick.prev();
    },

    reInit() {
      // Helpful if you have to deal with v-for to update dynamic lists
      this.$nextTick(() => {
        this.$refs.slick.reSlick();
      });
    },

    // Events listeners
    handleAfterChange(event, slick, currentSlide) {
      console.log("handleAfterChange", event, slick, currentSlide);
    },
    handleBeforeChange(event, slick, currentSlide, nextSlide) {
      console.log("handleBeforeChange", event, slick, currentSlide, nextSlide);
    },
    handleBreakpoint(event, slick, breakpoint) {
      console.log("handleBreakpoint", event, slick, breakpoint);
    },
    handleDestroy(event, slick) {
      console.log("handleDestroy", event, slick);
    },
    handleEdge(event, slick, direction) {
      console.log("handleEdge", event, slick, direction);
    },
    handleInit(event, slick) {
      console.log("handleInit", event, slick);
    },
    handleReInit(event, slick) {
      console.log("handleReInit", event, slick);
    },
    handleSetPosition(event, slick) {
      console.log("handleSetPosition", event, slick);
    },
    handleSwipe(event, slick, direction) {
      console.log("handleSwipe", event, slick, direction);
    },
    handleLazyLoaded(event, slick, image, imageSource) {
      console.log("handleLazyLoaded", event, slick, image, imageSource);
    },
    handleLazeLoadError(event, slick, image, imageSource) {
      console.log("handleLazeLoadError", event, slick, image, imageSource);
    },    
    submitHandlerStepOne () { 
      this.$v.firstName.$touch()
      this.$v.lastName.$touch()
      if (!this.$v.firstName.$invalid && !this.$v.lastName.$invalid) {
        this.stepOne = false;
        this.stepSecond = true;
        this.validStepTwo = true;
      }              
    },
    submitHandlerStepTwo () { 
      this.$v.email.$touch()
      this.$v.password.$touch()
      this.$v.repeatPassword.$touch()
      if (!this.$v.email.$invalid && !this.$v.password.$invalid && !this.$v.repeatPassword.$invalid) {
        this.stepOne = false;
        this.stepSecond = false;
      }      
    },
    submitHandlerStepThree () { 
      if (this.$v.$invalid) {
        this.$v.$touch()        
        return
      }       
    },  
    submitHandler () { 
      if (this.$v.$invalid) {
        this.$v.$touch()        
        return
      }       
    },  
    startTimer () {  
      this.$v.phone.$touch()
      if (!this.$v.phone.$invalid) {
        let vm = this;      
        this.resend = true;       
        if (this.valueProgress >= 1) {        
          this.mainTimer = setInterval(function() {            
            vm.valueProgress--;
            if (vm.valueProgress === 0) {
              clearInterval(vm.mainTimer);
              clearInterval(vm.resetTimer);       
            }          
          }, 1000);
        }
      }
    },
    resendTimer () {
      clearInterval(this.mainTimer);
      this.$v.phone.$touch()
      if (!this.$v.phone.$invalid) {
        let vm = this;    
        this.valueProgress = 60;
        this.disabled = true;
        if (this.valueProgress >= 1) {  
          clearInterval(this.resetTimer);       
          this.resetTimer = setInterval(function() {
            vm.valueProgress--;
            if (vm.valueProgress === 0) {
              vm.disabled = false;
              clearInterval(vm.mainTimer);
              clearInterval(vm.resetTimer);        
            }          
          }, 1000);
        }
      }
    },
    submitRegistration () {
      this.$v.code.$touch()
      if (!this.$v.code.$invalid) {
        window.location.href = 'VIEW-1.html';
      }
    },
    goStepOne () {
      this.stepOne = true;
      this.stepSecond = false;
    },
    goStepSecond () {
      this.stepOne = false;
      this.stepSecond = true;
    },
    goStepThree () {
      this.stepOne = false;
      this.stepSecond = false;
    },
    showModal(item) {
      this.idModal = item;
      this.$root.$emit('bv::show::modal', 'modal-1', '#btnShow');
    },
  },
  created() {
    window.addEventListener("resize", this.handleResize);
    this.handleResize();
  },
  destroyed() {
    window.removeEventListener("resize", this.handleResize);
  },
  mounted: function () {
    loadYmap({ ...settings, debug: true });
    this.loading = false    
  }
});
