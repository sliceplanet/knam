'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var webpackConfig = require('./webpack.config.js');
var webpack = require('webpack-stream');

gulp.task('default', function () {
  // place code for your default task here
});

gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
})

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', gulp.parallel('sass'));
});

gulp.task('webpack:watch', function () {
  let conf = webpackConfig;
  conf.watch = true;

  return gulp.src('./js/**/*.js')
    .pipe(webpack(webpackConfig))
    .pipe(gulp.dest('./dist'));
});


gulp.task('watch', gulp.parallel('sass:watch', 'webpack:watch'));
